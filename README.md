# Setting Custom CSS on vs-code Markdown Preview

## Overview

In this lab, we will be going through on how to set up Custom CSS Markdown Preview on Visual Studio Code.

## Prerequisites

* Make sure you already had Visual Studio Code installed in your machine.

## Step by step

### Deactivate Default CSS Files of your VS-Code

1. Open the markdown file that you'd want to preview on VS-Code, in this tutorial we will be using the sample repository's [README.md](./README.md)

![](./images/picture1.png)

2. Then type shortcut **ctrl + shift + v** (Windows) or **cmd + shift + v** (Mac) to preview your markdown file on the built-in markdown previewer on VS-Code.

![](./images/picture2.png)

3. Then type shortcut **ctrl + shift + p** (Windows) or **cmd + shift + p** (Mac) to open Command Pallete and then search for __Developer: OpenWebview Developer Tools__

![](./images/picture3.png)

4. Navigate to **Network** Tab and search for a file called **markdown.css**

![](./images/picture4.png)

5. Navigate to it's location in the your computer manually and rename it to markdown.cssbu

### Install Custom CSS Files to your VS-Code

1. Reopen the markdown file that you'd want to preview on VS-Code.

![](./images/picture5.png)

2. Then type shortcut **ctrl + ,** (Windows) or **cmd + ,** (Mac) to open settings, and then search for **markdown styles**

![](./images/picture6.png)

3. Click on Add Item, then add in the `.vscode/custom.css`

![](./images/picture7.png)

4. Then create a folder called `.vscode` in your own repo and copy [custom.css](./custom.css) to .vscode in your own md repo.

![](./images/picture8.png)

5. Then congratulations your Markdown Preview is already set up correctly!

![](./images/picture9.png)

## Conclusion

After finishing this tutorial you would learn had learn that vs-code is just an Electron App, and that you can set up your own custom css styles to preview your own markdown files.

## Clean up

If somehow you want to turn it back to the original one, just delete the custom.css defition from your settings and rename **markdown.cssbu** back to **markdown.css**
